#!/usr/bin/env julia

using PyCall
using JLD
using AcousticModels
using FEATools
using DelimitedFiles

meshio = pyimport("meshio")

function read_fem_eigenfrequencies(file_name::String)
    # Read, flatten the array and transform eigenvalues in eigenfrequencies.
    # Negative eigenvalues are artefacts and are replaced with zeros.
    data= readdlm(file_name)[:]
    data[data .< 0] .= 0
    return sqrt.(data) / 2π
end

function load_mesh(file_name::String)
    return meshio.read(file_name)
end

function write_mesh(file_name::String, mesh::PyObject)
    meshio.write(file_name, mesh)
end

function validate(
    Lx::Real,
    Ly::Real,
    Lz::Real,
    N::Int
    )

    eigen_fem = read_fem_eigenfrequencies("elmerfem/eigenvalues.dat")

    f_min = minimum(eigen_fem)
    n_modes = length(eigen_fem)

    A, B, C = index_grid(N, N, N)
    F = mode_frequency.(A, B, C, Lx, Ly, Lz)
    eigen_exact = sort(unique(F))
    eigen_exact = eigen_exact[eigen_exact .>= f_min][1:n_modes]

    eigen_error_cents = 1200 * log2.(eigen_fem ./ eigen_exact)

    fem_data = load_mesh("elmerfem/case_t0001.vtu")
    exact_data = load_mesh("elmerfem/case_t0001.vtu")
    expand_data = load_mesh("elmerfem/case_t0001.vtu")
    expand_exact_data = load_mesh("elmerfem/case_t0001.vtu")
    expand_c_data = load_mesh("elmerfem/case_t0001.vtu")
    expand_diff_data = load_mesh("elmerfem/case_t0001.vtu")
    norms = zeros(length(eigen_exact))
    signs = zeros(length(eigen_exact))
    # corrs = zeros(length(eigen_exact))

    vtu_fields = Dict()
    exact_vtu_fields = Dict()

    for n in 1:length(eigen_exact)

        fem_shape = fem_data.point_data["pressure EigenMode" * string(n)]

        expand_data_fields = Dict("pressure EigenMode" => fem_shape)
        expand_data.point_data = expand_data_fields
        write_mesh("elmerfem/case_expanded_t" * lpad(n, 4, "0") * ".vtu", expand_data)

        exact_shape = mode.(
            fem_data.points[:, 1],
            fem_data.points[:, 2],
            fem_data.points[:, 3],
            A[F .== eigen_exact[n]][1],
            B[F .== eigen_exact[n]][1],
            C[F .== eigen_exact[n]][1],
            Lx,
            Ly,
            Lz
        )

        exact_vtu_fields["pressure EigenMode" * string(n)] = exact_shape

        expand_exact_data_fields = Dict("mode" => exact_shape)
        expand_exact_data.point_data = expand_exact_data_fields
        write_mesh("elmerfem/exact_expanded_t" * lpad(n, 4, "0") * ".vtu", expand_exact_data)

        max_idx = argmax(exact_shape)

        signs[n] = sign(exact_shape[max_idx] / fem_shape[max_idx])

        expand_c_data_fields = Dict("mode" => signs[n] * fem_shape)
        expand_c_data.point_data = expand_c_data_fields
        write_mesh("elmerfem/case_c_expanded_t" * lpad(n, 4, "0") * ".vtu", expand_c_data)

        # corrs[n] = exact_shape[max_idx] / fem_shape[max_idx]

        diff_field = signs[n] * fem_shape - exact_shape

        norms[n] = sqrt(sum(abs2.(diff_field))) / (Lx * Ly * Lz)

        vtu_fields["diff EigenMode" * string(n)] = diff_field

        expand_diff_data_fields = Dict("diff" => diff_field)
        expand_diff_data.point_data = expand_diff_data_fields
        write_mesh("elmerfem/check_expanded_t" * lpad(n, 4, "0") * ".vtu", expand_diff_data)

    end

    fem_data.point_data = vtu_fields
    write_mesh("elmerfem/check_t0001.vtu", fem_data)

    exact_data.point_data = exact_vtu_fields
    write_mesh("elmerfem/exact_t0001.vtu", exact_data)

    return eigen_error_cents, norms, signs

end

eigen_error_cents, norms, signs = validate(5, 4, 3, 100)
save(
    "validation_data.jld",
    "eigen_error_cents", eigen_error_cents,
    "norms", norms,
    "signs", signs
    )

